# EzCSR

This module simplifies the creation of a CSR1KV in AWS with with External Connections to Aviatrix Gateways for simulating on-prem environments. The resources created by the module are pictured below:

![Topology](ezCSR_Topology.png)

# Instructions

This Module requires the AWS Provider and Aviatrix Providers to be defined and configured in the root module. The Module accepts Aviatrix Transit Gateway data sources as inputs and automatically creates the external connections on the Aviatrix Controller and configures the CSR with the correct crypto settings and Tunnels. Module invocation looks like so:

```terraform
# Optionally, create multiple aliased providers to deploy multiple CSRs across regions
provider "aws" {
	alias   = "usw1"
	region  = "us-west-1"
}

provider "aws" {
	alias   = "usw2"
	region  = "us-west-2"
}

# Create data sources for the Aviatrix Gateways
data "aviatrix_transit_gateway" "demo_transit" {
    gw_name = "Test-Transit"
}

data "aviatrix_transit_gateway" "demo_transit_west" {
    gw_name = "TestWest-Transit"
}

# Call the Module and feed in the necessary info and data sources as a list
module "OnPremWest1" {
  source			= "./ezCSR"
  providers			= { aws = aws.usw1 }
  hostname			= "CSROnprem-West1"
  public_key			= "ssh-rsa ..."
  vpc_cidr			= "172.16.0.0/16"
  public_sub			= "172.16.0.0/24"
  private_sub			= "172.16.1.0/24"
  instance_type			= "t2.medium"
  avtx_gateways			= [ data.aviatrix_transit_gateway.demo_transit, data.aviatrix_transit_gateway.demo_transit_west ]
  avtx_gw_bgp_as_num		= [ "64525", "64526" ]
  csr_bgp_as_num		= "64527"
  create_client			= true
}

module "OnPremWest2" {
  source			= "./ezCSR"
  providers			= { aws = aws.usw2 }
  hostname			= "CSROnprem-West2"
  public_key			= "ssh-rsa ..."
  vpc_cidr			= "172.24.0.0/16"
  public_sub			= "172.24.0.0/24"
  private_sub			= "172.24.1.0/24"
  instance_type			= "t2.medium"
  avtx_gateways			= [ data.aviatrix_transit_gateway.demo_transit, data.aviatrix_transit_gateway.demo_transit_west ]
  avtx_gw_bgp_as_num		= [ "64525", "64526" ]
  csr_bgp_as_num		= "64528"
  create_client			= true
  private_ips			= true
}
```

Example of getting outputs:

```terraform
output "CSRWest1_Pub_IP" {
  value = module.OnPremWest1.public_ip
}
output "CSRWest1_Hostname" {
  value = module.OnPremWest1.hostname
}
```

Explanation of module arguments:

- **hostname:** The hostname which will be configured on the CSR and which will prefix all of the resources created.
- **public_key:** This can be either a valid public key if you have an existing private key and want to use it to log in or a placeholder as the CSR is created with an admin user with the password 'Password123'.
- **vpc_cidr:** The VPC CIDR block to use when creating the VPC which the CSR will reside in.
- **public_sub:** The public subnet for the CSR public facing interface.
- **private_sub:** The private subnet for the CSR private facing interface. If enabled, the test client will be created in this subnet.
- **instance_type (optional):** The instance type to launch the CSR with. Default is t2.medium
- **avtx_gateways:** List of Aviatrix Gateway data sources. External Connections will be created automatically for each gateway and Tunnels will be built to that gateway and it's HA peer gateway, if it exists.
- **avtx_gw_bgp_as_num:** List of BGP AS numbers to use on the Aviatrix Gateway side. Order matters here, it should match with the order of the list of gateway data sources.
- **csr_bgp_as_num:** BGP AS Number to use on the CSR.
- **create_client (optional):** If enabled, creates an amazon linux instance in the private subnet, configures public SG to allow port 2222 and configures a port forward on the CSR to allow SSH into the test instance using the CSR EIP and port 2222. SSH Key used for the instance will be the one defined earlier. Disabled by default.
- **private_ips:** Whether or not to use private ip addresses to build the external connection (e.g. if you have DX/ER or other underlay)

Module outputs:

- **public_ip:** Public EIP of CSR instance
- **hostname:** Hostname of CSR Instance
