output "hostname" {
  value = var.hostname
}
output "public_ip" {
  value = aws_eip.csr_public_eip[0].public_ip
}