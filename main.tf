terraform {
	required_providers {
		aws = {
			source = "hashicorp/aws"
			version = ">= 3.25"
		}
		aviatrix = {
			source = "AviatrixSystems/aviatrix"
			version = ">= 2.15"
		}
	}
}

data "aws_region" "current" {}

locals {
	csr_aws_ami_id = {
		us-east-1 =  "ami-0096dff105d15737b",
		us-east-2 =  "ami-062d9a2d78f5a83b5",
		us-west-1 =  "ami-0aa14429cefd8c85f",
		us-west-2 =  "ami-076cd1de76b34bcb1",
		us-gov-west-1 =  "ami-0ad275dc74f99f6a6",
		us-gov-east-1 =  "ami-05efd62b29cb209bd",
		eu-central-1 =  "ami-07f45aec881bfceee",
		ca-central-1 =  "ami-0c78e9398031db6ec",
		eu-west-1 =  "ami-0bed0e3b4ce5ebfd3",
		eu-west-2 =  "ami-0149f0f86473f12c6",  
		eu-west-3 =  "ami-0c4f1786fd3bdf3e6",    			
		ap-southeast-1 =  "ami-04480b1c4836e77e9",              
		ap-southeast-2 =  "ami-0b4a6d18bfb99b3db",   
		ap-south-1 =  "ami-00f83a2b517b361d5",
		ap-northeast-1 =  "ami-074a495744cac8924",  
		ap-northeast-2 =  "ami-072d8f981a3b80447",
		ap-east-1 =  "ami-0175e50af54c8cc36",
		sa-east-1 =  "ami-0a19abf35c2153085",
		eu-north-1 =  "ami-02203055e571d6732",	
		eu-south-1 =  "ami-0a20d637f1d507140",	
		af-south-1 =  "ami-0ae63b41ae46f9606", 			
		me-south-1 =  "ami-09f357575fcb8e733"
	}

  gateways = {for gw in var.avtx_gateways: gw.gw_name => gw}

}

#Create AWS VPC and Subnets
resource "aws_vpc" "csr_aws_vpc" {
  count = var.aws_deploy_csr == "true" ? 1 : 0
  cidr_block = var.vpc_cidr
}

resource "aws_subnet" "csr_aws_public_subnet" {
  count = var.aws_deploy_csr == "true" ? 1 : 0
  vpc_id = aws_vpc.csr_aws_vpc.*.id[count.index]
  cidr_block = var.public_sub
  map_public_ip_on_launch = false
  availability_zone = "${data.aws_region.current.name}a"

  tags = {
	"Name" = "${var.hostname} Public Subnet"
  }
}

resource "aws_subnet" "csr_aws_private_subnet" {
  count = var.aws_deploy_csr == "true" ? 1 : 0
  vpc_id = aws_vpc.csr_aws_vpc.*.id[count.index]
  cidr_block = var.private_sub
  map_public_ip_on_launch = false
  availability_zone = "${data.aws_region.current.name}a"
  
  tags = {
	"Name" = "${var.hostname} Private Subnet"
  }
}

#Create IGW for public subnet
resource "aws_internet_gateway" "csr_igw" {
  count = var.aws_deploy_csr == "true" ? 1 : 0
  vpc_id = aws_vpc.csr_aws_vpc.*.id[count.index]

  tags = {
	"Name" = "${var.hostname} Public Subnet IGW"
  }
}

#Create AWS Public and Private Subnet Route Tables
resource "aws_route_table" "csr_public_rtb" {
  count = var.aws_deploy_csr == "true" ? 1 : 0
  vpc_id = aws_vpc.csr_aws_vpc.*.id[count.index]

  tags = {
	"Name" = "${var.hostname} Public Route Table"
  }
}

resource "aws_route_table" "csr_private_rtb" {
  count = var.aws_deploy_csr == "true" ? 1 : 0
  vpc_id = aws_vpc.csr_aws_vpc.*.id[count.index]

  tags = {
	"Name" = "${var.hostname} Private Route Table"
  }
}

resource "aws_route" "csr_public_default" {
  count = var.aws_deploy_csr == "true" ? 1 : 0
  route_table_id = aws_route_table.csr_public_rtb.*.id[count.index]
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.csr_igw.*.id[count.index]
  depends_on = [ aws_route_table.csr_public_rtb, aws_internet_gateway.csr_igw ]
}

resource "aws_route" "csr_private_default" {
  count = var.aws_deploy_csr == "true" ? 1 : 0
  route_table_id = aws_route_table.csr_private_rtb.*.id[count.index]
  destination_cidr_block = "0.0.0.0/0"
  network_interface_id = aws_network_interface.CSR_Private_ENI.*.id[count.index]
  depends_on = [ aws_route_table.csr_private_rtb, aws_instance.CSROnprem, aws_network_interface.CSR_Private_ENI ]
}

resource "aws_route_table_association" "csr_public_rtb_assoc" {
  count = var.aws_deploy_csr == "true" ? 1 : 0
  subnet_id = aws_subnet.csr_aws_public_subnet.*.id[count.index]
  route_table_id = aws_route_table.csr_public_rtb.*.id[count.index]
}

resource "aws_route_table_association" "csr_private_rtb_assoc" {
  count = var.aws_deploy_csr == "true" ? 1 : 0
  subnet_id = aws_subnet.csr_aws_private_subnet.*.id[count.index]
  route_table_id = aws_route_table.csr_private_rtb.*.id[count.index]
}

resource "aws_security_group" "csr_public_sg" {
  count = var.aws_deploy_csr == "true" ? 1 : 0
  name = "csr_public"
  description = "Security group for public CSR ENI"
  vpc_id = aws_vpc.csr_aws_vpc.*.id[count.index]

  tags = {
	"Name" = "${var.hostname} Public SG"
  }
}

resource "aws_security_group" "csr_private_sg" {
  count = var.aws_deploy_csr == "true" ? 1 : 0
  name = "csr_private"
  description = "Security group for private CSR ENI"
  vpc_id = aws_vpc.csr_aws_vpc.*.id[count.index]

  tags = {
	"Name" = "${var.hostname} Private SG"
  }
}

resource "aws_security_group_rule" "csr_public_ssh" {
  count = var.aws_deploy_csr == "true" ? 1 : 0
  type = "ingress"
  from_port = 22
  to_port = 22
  protocol = "tcp"
  cidr_blocks = [ "0.0.0.0/0" ]
  security_group_id = aws_security_group.csr_public_sg.*.id[count.index]
}

resource "aws_security_group_rule" "client_forward_ssh" {
  count = var.create_client ? 1 : 0
  type = "ingress"
  from_port = 2222
  to_port = 2222
  protocol = "tcp"
  cidr_blocks = [ "0.0.0.0/0" ]
  security_group_id = aws_security_group.csr_public_sg.*.id[count.index]
}

resource "aws_security_group_rule" "csr_public_dhcp" {
  count = var.aws_deploy_csr == "true" ? 1 : 0
  type = "ingress"
  from_port = 67
  to_port = 67
  protocol = "udp"
  cidr_blocks = [ "0.0.0.0/0" ]
  security_group_id = aws_security_group.csr_public_sg.*.id[count.index]
}

resource "aws_security_group_rule" "csr_public_ntp" {
  count = var.aws_deploy_csr == "true" ? 1 : 0
  type = "ingress"
  from_port = 123
  to_port = 123
  protocol = "udp"
  cidr_blocks = [ "0.0.0.0/0" ]
  security_group_id = aws_security_group.csr_public_sg.*.id[count.index]
}

resource "aws_security_group_rule" "csr_public_snmp" {
  count = var.aws_deploy_csr == "true" ? 1 : 0
  type = "ingress"
  from_port = 161
  to_port = 161
  protocol = "udp"
  cidr_blocks = [ "0.0.0.0/0" ]
  security_group_id = aws_security_group.csr_public_sg.*.id[count.index]
}

resource "aws_security_group_rule" "csr_public_esp" {
  count = var.aws_deploy_csr == "true" ? 1 : 0
  type = "ingress"
  from_port = 500
  to_port = 500
  protocol = "udp"
  cidr_blocks = [ "0.0.0.0/0" ]
  security_group_id = aws_security_group.csr_public_sg.*.id[count.index]
}

resource "aws_security_group_rule" "csr_public_ipsec" {
  count = var.aws_deploy_csr == "true" ? 1 : 0
  type = "ingress"
  from_port = 4500
  to_port = 4500
  protocol = "udp"
  cidr_blocks = [ "0.0.0.0/0" ]
  security_group_id = aws_security_group.csr_public_sg.*.id[count.index]
}

resource "aws_security_group_rule" "csr_public_egress" {
  count = var.aws_deploy_csr == "true" ? 1 : 0
  type = "egress"
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = [ "0.0.0.0/0" ]
  security_group_id = aws_security_group.csr_public_sg.*.id[count.index]
}

resource "aws_security_group_rule" "csr_private_ingress" {
  count = var.aws_deploy_csr == "true" ? 1 : 0
  type = "ingress"
  from_port = 0
  to_port = 0
  protocol = -1
  cidr_blocks = [ "0.0.0.0/0" ]
  security_group_id = aws_security_group.csr_private_sg.*.id[count.index]
}

resource "aws_security_group_rule" "csr_private_egress" {
  count = var.aws_deploy_csr == "true" ? 1 : 0
  type = "egress"
  from_port = 0
  to_port = 0
  protocol = -1
  cidr_blocks = [ "0.0.0.0/0" ]
  security_group_id = aws_security_group.csr_private_sg.*.id[count.index]
}

resource "aws_network_interface" "CSR_Public_ENI" {
  count = var.aws_deploy_csr == "true" ? 1 : 0
  subnet_id = aws_subnet.csr_aws_public_subnet.*.id[count.index]
  security_groups = [ aws_security_group.csr_public_sg.*.id[count.index] ]
  source_dest_check = false

  tags = {
	"Name" = "${var.hostname} Public Interface"
  }
}

resource "aws_network_interface" "CSR_Private_ENI" {
  count = var.aws_deploy_csr == "true" ? 1 : 0
  subnet_id = aws_subnet.csr_aws_private_subnet.*.id[count.index]
  security_groups = [ aws_security_group.csr_private_sg.*.id[count.index] ]
  source_dest_check = false

  tags = {
	"Name" = "${var.hostname} Private Interface"
  }
}

resource "aws_eip" "csr_public_eip" {
  count = var.aws_deploy_csr == "true" ? 1 : 0
  vpc = true
  network_interface	= aws_network_interface.CSR_Public_ENI.*.id[count.index]
  depends_on = [ aws_internet_gateway.csr_igw ]

  tags = {
	"Name" = "${var.hostname} Public IP"
  }
}

resource "aws_key_pair" "csr_deploy_key" {
  count = var.aws_deploy_csr == "true" ? 1 : 0
  key_name = "${var.hostname}_sshkey"
  public_key = var.public_key
}

resource "aviatrix_transit_external_device_conn" "ezCSRConn" {
  count               = length(var.avtx_gateways)
	vpc_id            	= var.avtx_gateways[count.index].vpc_id
	connection_name   	= "${var.hostname}_to_${var.avtx_gateways[count.index].gw_name}"
	gw_name           	= var.avtx_gateways[count.index].gw_name
	connection_type   	= "bgp"
	bgp_local_as_num  	= var.avtx_gw_bgp_as_num[count.index]
	bgp_remote_as_num 	= var.csr_bgp_as_num
	ha_enabled = false
  direct_connect      = var.private_ips
	remote_gateway_ip 	= aws_eip.csr_public_eip[0].public_ip
	pre_shared_key      = "aviatrix"
}

data "aws_ami" "amazon-linux" {
  count = var.create_client ? 1 : 0
  most_recent = true
  owners = ["amazon"]
  
  filter {
    name = "name"
    values = ["amzn2-ami-hvm*"]
  }
}

resource "aws_instance" "test_client" {
  count = var.create_client ? 1 : 0
  ami = data.aws_ami.amazon-linux.*.id[count.index]
  instance_type = "t2.micro"
  key_name = "${var.hostname}_sshkey"
  subnet_id = aws_subnet.csr_aws_private_subnet[0].id
  vpc_security_group_ids = [ aws_security_group.csr_private_sg[0].id ]
  associate_public_ip_address = false

  tags = {
    "Name" = "TestClient_${var.hostname}"
  }
}

data "aws_network_interface" "test_client_if" {
  count = var.create_client ? 1 : 0
  id = aws_instance.test_client[count.index].primary_network_interface_id
}

resource "aws_instance" "CSROnprem" {
  count = var.aws_deploy_csr == "true" ? 1 : 0
  ami = local.csr_aws_ami_id[data.aws_region.current.name]
  instance_type = var.instance_type
  key_name = "${var.hostname}_sshkey"

  network_interface {
	network_interface_id = aws_network_interface.CSR_Public_ENI.*.id[count.index]
	device_index = 0
  }

  network_interface {
	network_interface_id = aws_network_interface.CSR_Private_ENI.*.id[count.index]
	device_index = 1
  }

  user_data = templatefile("${path.module}/csr.sh",{
	  external_conns = aviatrix_transit_external_device_conn.ezCSRConn
	  gateway = local.gateways
    hostname = var.hostname
    test_client_ip = var.create_client ? data.aws_network_interface.test_client_if[0].private_ip : ""
  })

  tags = {
	"Name" = var.hostname
  }
}